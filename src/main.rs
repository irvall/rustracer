use math::vec3;
use math::ray;
fn main() {

    let nx = 200;
    let ny = 100;
    println!("P3\n{} {}\n255", nx, ny);

    let llc = vec3::new_vec(-2., -1., -1.);
    let horizontal = vec3::new_vec(4., 0., 0.);
    let vertical = vec3::new_vec(0., 2., 0.);
    let origin = vec3::new_vec(0., 0., 0.);

    let j_rng = (0..ny).rev();
    for j in j_rng {
        for i in 0..nx {
            let u = i as f64 / nx as f64;
            let v = j as f64 / ny as f64;

            let r = ray::new_ray(origin, llc + horizontal*u + vertical*v);
            let col = color(r);
            let res = col * 255.99;
            println!("{} {} {}", res.x() as i32, res.y() as i32, res.z() as i32);
        }
    }

}


fn color(r: ray::Ray) -> vec3::Vec3 {
    let unit_direction = r.dir().unit();
    let t = 0.5 * (unit_direction.y() + 1.);
    vec3::new_vec(1.,1.,1.)*(1.-t) + (vec3::new_vec(0.5, 0.7, 1.)*t)
}
